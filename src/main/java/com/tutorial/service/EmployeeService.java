package com.tutorial.service;

import com.tutorial.model.Employee;

public interface EmployeeService {
    Employee getEmployeeByName(String name);
}
