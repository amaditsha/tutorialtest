package com.tutorial.service.impl;

import com.tutorial.model.Employee;
import com.tutorial.repository.EmployeeRepository;
import com.tutorial.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee getEmployeeByName(String name) {
        return employeeRepository.findEmployeeByName(name);
    }
}
