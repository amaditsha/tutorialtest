package com.tutorial;

import com.tutorial.model.Employee;
import com.tutorial.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

//@SpringBootTest
@DataJpaTest
public class EmployeeRepositoryIntegrationTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void whenFindByName_thenReturnEmployee() {
//        Given
        Employee alex = new Employee();
        alex.setName("Alex");
        entityManager.persist(alex);
        entityManager.flush();

//        When
        Employee found = employeeRepository.findEmployeeByName(alex.getName());

//        then
        assertThat(found.getName()).isEqualTo(alex.getName());
    }
}
